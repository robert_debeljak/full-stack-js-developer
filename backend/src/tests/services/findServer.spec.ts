import request = require('supertest');
import { SuperTest, Test } from 'supertest';
import { Server } from '../../models/server';
import axios from 'axios';

jest.setTimeout(5000);

let agent: SuperTest<Test>;

const servers: Server[] = [
  {
    url: 'https://does-not-work.perfume.new',
    priority: 1
  },
  {
    url: 'https://gitlab.com',
    priority: 4
  },
  {
    url: 'http://app.scnt.me',
    priority: 3
  },
  {
    url: 'https://offline.scentronix.com',
    priority: 2
  }
];
