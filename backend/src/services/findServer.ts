import axios from 'axios';
import { Server } from '../models/server';

const servers: Server[] = [
  {
    url: 'https://does-not-work.perfume.new',
    priority: 1
  },
  {
    url: 'https://gitlab.com',
    priority: 4
  },
  {
    url: 'http://app.scnt.me',
    priority: 3
  },
  {
    url: 'https://offline.scentronix.com',
    priority: 2
  }
];

/**
 * Send get request to the specific server and estimate if it is online or offline.
 */
const postRequestToServer = async (server: Server): Promise<boolean> => {
  try {
    setTimeout(() => {
      return false;
    }, 5000);

    const res = await axios.get(server.url);

    return res.status >= 200 && res.status < 300;
  } catch (err) {
    const { response } = {...err} as any;
    if (response) {
      return response.status >= 200 && response.status < 300;
    } else {
      return false;
    }
  }
};

/**
 * Find server what is online or reject error when all servers are offline.
 */
export const findServer = async () => {
  return Promise.all(servers.map((server) => postRequestToServer(server).then((res) => {
    server.online = res;
  }))).then(() => {
    let result: Server;

    for (const server of servers) {
      if (server.online && (!result || result.priority > server.priority)) {
        result = server;
      }
    }

    if (result) {
      return Promise.resolve({ url: result.url, priority: result.priority });
    } else {
      return Promise.reject('No servers are online');
    }
  });
};
