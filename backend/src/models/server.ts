export interface Server {
  url: string;
  priority: number;
  online?: boolean;
}
