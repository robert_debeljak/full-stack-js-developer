import { createStyles, Theme } from '@material-ui/core';

export const headerStyles = ({ spacing, typography, palette }: Theme) =>
  createStyles({
    logoWrapper: {
      position: 'absolute',
      paddingLeft: 'calc((100% - 1200px) / 2)'
    },
    links: {
      margin: '0 auto',
      maxWidth: 1200,
      paddingLeft: 120
    },
    navLink: {
      marginRight: spacing(7),
      cursor: 'pointer',
      '&:hover, &.MuiLink-root.active': {
        borderBottom: `2px solid ${palette.error.main}`,
        textDecoration: 'none',
        marginTop: 2
      },
      textTransform: 'uppercase',
      letterSpacing: 1.5,
      fontSize: typography.pxToRem(14),
      fontWeight: typography.fontWeightBold,
      paddingBottom: 3
    },
    subNav: {
      backgroundColor: '#f8f5f0',
    },
    subNavLink: {
      marginRight: spacing(3),
      textTransform: 'uppercase',
      letterSpacing: 1.5,
      fontSize: typography.pxToRem(12),
      fontWeight: typography.fontWeightBold
    }
  });

export const layoutStyles = ({ spacing }: Theme) =>
  createStyles({
    container: {
      maxWidth: 1200,
      margin: '0 auto',
      padding: spacing(5)
    }
  });

export const breadcrumbStyles = ({ typography , palette}: Theme) =>
  createStyles({
    item: {
      textTransform: 'uppercase',
      fontSize: typography.pxToRem(12),
      fontWeight: typography.fontWeightBold,
      color: palette.primary.main,
      letterSpacing: 1.5
    }
  });

export const recipeCardStyles = ({ typography , spacing, palette}: Theme) =>
  createStyles({
    title: {
      fontWeight: typography.fontWeightBold,
      fontSize: '2.5rem',
      marginTop: spacing(2)
    },
    cardWrapper: {
      margin: '0 auto'
    },
    cardImage: {
      width: 600,
      height: 430,
      marginLeft: spacing(6),
      maxWidth: 'calc(100% - 96px)',
      objectFit: 'contain'
    },
    content: {
      fontSize: typography.pxToRem(16),
      marginTop: spacing(10)
    },
    detailPanel: {
      borderBottom: '1px solid #ededed',
      padding: `${spacing(2)}px 0`
    },
    detailItemLabel: {
      fontSize: typography.pxToRem(12),
      fontWeight: typography.fontWeightBold,
      textTransform: 'uppercase',
    },
    detailItemText: {
      fontSize: typography.pxToRem(17),
      fontWeight: typography.fontWeightBold
    },
    button: {
      border: `1px solid ${palette.error.main}`,
      borderRadius: 'unset',
      marginLeft: spacing(1),
      fontSize: typography.pxToRem(12),
      fontWeight: typography.fontWeightBold,
      textTransform: 'uppercase',
      padding: `${spacing(0.5)}px ${spacing(1)}px`,
      '& svg': {
        marginRight: spacing(1)
      }
    }
  });
