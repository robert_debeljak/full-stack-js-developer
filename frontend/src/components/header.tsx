import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { Box } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { headerStyles } from './styles';
import Link from './Link';

interface INavItem {
  name: string;
  link: string;
  subItems?: object[];
}

interface INavSubItem {
  name: string;
  link: string;
}

const navigationItems: INavItem[] = [
  {
    name: 'shop',
    link: '/shop'
  },
  {
    name: 'recipes',
    link: '/recipes',
    subItems: [
      {
        name: 'categories',
        link: '/'
      },
      {
        name: 'collections',
        link: '/'
      },
      {
        name: 'resources',
        link: '/'
      }
    ]
  },
  {
    name: 'learn',
    link: '/learn'
  },
  {
    name: 'about',
    link: '/about'
  },
  {
    name: 'blog',
    link: '/blog'
  }
];

const useStyles = makeStyles(headerStyles);

export const MainHeader: React.FC = () => {
  const [activeLinkId, setActiveLinkId] = useState<number | undefined>(undefined);
  const classes = useStyles();
  const router = useRouter();
  const Logo = '/img/logo.png';

  useEffect(() => {
    if (!router.pathname && router.pathname === '/') {
      setActiveLinkId(0);
    } else {
      navigationItems.forEach((navItem: INavItem, index: number) => {
        if (router.pathname.indexOf(navItem.link) > -1) {
          setActiveLinkId(index);
        }
      });
    }
  }, [router.pathname]);

  const navLinks = (
    <Box
      className={classes.links}
      display='flex'
      alignItems='center'
      height={75}
    >
      {navigationItems.map((item: INavItem, index: number) => (
        <>
          <Link
            key={`nav-${index}`}
            href={item.link}
            className={`${classes.navLink} ${router.pathname.indexOf(item.link) > -1 && 'active'}`}
          >
            {item.name}
          </Link>
        </>
      ))}
    </Box>
  );

  const subNavLinks = (
    activeLinkId !== undefined && navigationItems[activeLinkId].subItems?.length && (
      <Box
        className={classes.links}
        display='flex'
        alignItems='center'
        height={60}
      >
        {navigationItems[activeLinkId].subItems?.map((subItem: any, index: number) => (
          <Link
            key={`sub-nav-${index}`}
            href={subItem.link}
            className={classes.subNavLink}
          >
            {subItem.name}
          </Link>
        ))}
      </Box>
    )
  );

  return (
    <Box my={2} height={135}>
      <Box className={classes.logoWrapper}>
        <img src={Logo} />
      </Box>
      {navLinks}
      <Box className={classes.subNav}>{subNavLinks}</Box>
    </Box>
  );
};
