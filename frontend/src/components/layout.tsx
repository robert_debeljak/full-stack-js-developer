import React from 'react';
import { Box } from '@material-ui/core';
import { MainHeader } from './header';
import { layoutStyles } from './styles';
import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles(layoutStyles);

const Layout: React.FC = ({ children }) => {
  const classes = useStyles();

  return (
    <Box>
      <MainHeader />
      <Box className={classes.container}>
        {children}
      </Box>
    </Box>
  );
};

export default Layout;
