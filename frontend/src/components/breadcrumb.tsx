import { Breadcrumbs, Typography } from '@material-ui/core';
import { NavigateNext as NavigateNextIcon } from '@material-ui/icons';
import { makeStyles } from '@material-ui/styles';
import { useRouter } from 'next/router';
import Link from '../components/Link';
import { breadcrumbStyles } from './styles';
import { RED_COLOR } from '../theme';

const useStyles = makeStyles(breadcrumbStyles);

const Breadcrumb: React.FC = () => {
  const router = useRouter();
  const classes = useStyles();
  let path: string = router.asPath || '';
  if (path[0] === '/') {
    path = path.slice(1);
  }
  const labels: string[] = path.split('/').map((label: string) => label.replace('-', ' ')) || [];

  return (
    <Breadcrumbs
      separator={
        <NavigateNextIcon htmlColor={RED_COLOR.main} fontSize='small' />
      }
    >
      {labels.length > 1 ? labels.map((label: string, index: number) => (
        <>
          {index === 0 ? (
            <Link
              className={classes.item}
              key={index}
              href={`/${label}`}
            >
              {label}
            </Link>
          ) : (
            <Typography key={index} className={classes.item}>
              {label}
            </Typography>
          )}
        </>
      )) : null}
      <Typography />
    </Breadcrumbs>
  );
};

export default Breadcrumb;
