import Head from 'next/head';
import { Box, Heading } from 'rebass';

export const Error404Page: React.FC = () => {
  return (
    <>
      <Head>
        <title>404 Not Found</title>
      </Head>

      <Box>
        <Heading>Not Found</Heading>
      </Box>
    </>
  );
};
