import { Box, Typography, Grid, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { AccessTime, Add, Print } from '@material-ui/icons';
import Breadcrumb from '../breadcrumb';
import { recipeCardStyles } from '../styles';

interface CardProps {
  data: IRecipeData;
}

interface IRecipeData {
  title: string;
  image: string;
  content: React.ReactElement<any, any>;
  details: IRecipeDetail;
  yield: string;
}

interface IRecipeDetail {
  prep: string;
  bake?: string;
  cook?: string;
  total: string;
}

const useStyles = makeStyles(recipeCardStyles);
const YieldIcon = "/icons/yield-icon.png";

const RecipeCard: React.FC<CardProps> = ({data}) => {
  const classes = useStyles();

  const detailPanel = (data: any) => (
    <Grid
      container
      spacing={2}
      className={classes.detailPanel}
    >
      <Grid item>
        <AccessTime fontSize='large' />
      </Grid>
      {Object.keys(data).map((key: string, index: number) => (
        <Grid item key={`card-detail-item-${index}`}>
          <Typography className={classes.detailItemLabel}>{key}</Typography>
          <Typography className={classes.detailItemText}>{data[key]}</Typography>
        </Grid>
      ))}
    </Grid>
  );

  const yieldPanel = (data: string) => (
    <Grid container spacing={2} style={{padding: `16px 0`}}>
      <Grid item>
        <img src={YieldIcon} width={35} height={35} />
      </Grid>
      <Box flex={1}>
        <Typography className={classes.detailItemLabel}>yield</Typography>
        <Typography className={classes.detailItemText}>{data}</Typography>
      </Box>
      <Grid item>
        <Button onClick={() => {}} className={classes.button}>
          <Add />
          SAVE RECIPE
        </Button>
        <Button onClick={() => {}} className={classes.button}>
          <Print />
          PRINT
        </Button>
      </Grid>
    </Grid>
  );

  return (
    <Box display='flex' flexWrap='wrap' className={classes.cardWrapper}>
      <Box flex={1} minWidth={400}>
        <Breadcrumb />
        {data !== undefined && (
          <>
            <Typography variant='h3' className={classes.title}>
              {!!data.title && data.title}
            </Typography>
            <Box mt={15} className={classes.content}>{!!data.content && data.content}</Box>
            {!!data.details && detailPanel(data.details)}
            {!!data.yield && yieldPanel(data.yield)}
          </>
        )}
      </Box>
      {data !== undefined && <img src={data.image} className={classes.cardImage} />}
    </Box>
  )
};

export default RecipeCard;
