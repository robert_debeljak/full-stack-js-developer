import { createMuiTheme } from '@material-ui/core/styles';

export const RED_COLOR = {
  main: '#a35c62'
};

// Create a theme instance.
const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#141414',
    },
    secondary: {
      main: '#19857b',
    },
    error: RED_COLOR,
    background: {
      default: '#fff',
    }
  },
});

export default theme;
