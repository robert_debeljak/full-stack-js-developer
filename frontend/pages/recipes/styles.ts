import { createStyles, Theme } from '@material-ui/core';

export const recipeListStyles = ({ typography }: Theme) =>
  createStyles({
    listItemLabel: {
      textTransform: 'capitalize',
      fontSize: typography.pxToRem(18)
    }
  });
