import { List, ListItem, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { useRouter } from 'next/router';
import Link from '../../src/components/Link';
import { recipeListStyles } from './styles';

interface IListItem {
  name: string;
  link: string;
}

const useStyles = makeStyles(recipeListStyles);

const Recipes = () => {
  const classes = useStyles();
  const router = useRouter();

  const categories = [
    {
      name: 'bread',
      link: '/bread/quick-bread'
    },
    {
      name: 'angel cake',
      link: '/bread/castella'
    },
    {
      name: 'salads',
      link: '/cook/salads'
    }
  ];

  return (
    <List>
      {categories.map((category: IListItem, index: number) => (
        <ListItem key={index}>
          <Link href={`${router.pathname}${category.link}`}>
            <Typography className={classes.listItemLabel}>{category.name}</Typography>
          </Link>
        </ListItem>
      ))}
    </List>
  )
};

export default Recipes;
