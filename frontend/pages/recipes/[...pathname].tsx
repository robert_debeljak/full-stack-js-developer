import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { Box } from '@material-ui/core';
import { withStyles } from '@material-ui/styles';
import { Error404Page } from '../../src/components/404';
import RecipeCard from '../../src/components/recipes/RecipeCard';
import Link from '../../src/components/Link';

interface Props {
  pageType: any;
  data: any;
  error404: boolean;
}

const StyledLink = withStyles((theme) => ({
  root: {
    textDecoration: 'underline'
  }
}))(Link);

const categories: any = {
  'quick-bread': {
    title: 'Whole-Grain Banana Bread',
    image: '/img/bread.png',
    content: (
      <Box>
        This one-bowl banana bread - our <StyledLink href='/'>2018 Recipe of the Year</StyledLink> - uses the simplest ingredients, but is incredibly moist and flavorful.<br/>
        While the recipe calls for a 50/50 mix of flours (all-purpose and whole wheat), we often make the bread 100% whole wheat, and honestly? No one can tell, it's that good! And not only is this bread delicious - it's versatile.
      </Box>
    ),
    details: {
      prep: '10 mins',
      bake: '1 hr to 1 hr 15 mins',
      total: '1 hr 10 mins'
    },
    yield: '1 loaf, 12 generous servings'
  },
  castella: {
    title: 'Fruit angel cake',
    image: '/img/castella.jpg',
    content: (
      <Box>
        This one-bowl fruit angel cake - our <StyledLink href='/'>2019 Recipe of the Year</StyledLink> - uses the natural ingredients, but is incredibly fragrant.<br/>
        We often make the cake egg and wheat, and honestly? No one can tell, it's that good! And not only is this cake delicious - it's flavourful.
      </Box>
    ),
    details: {
      prep: '30 mins',
      bake: '1 hr to 1 hr 15 mins',
      total: '1 hr 30 mins'
    },
    yield: '1 loaf, 6 generous servings'
  },
  salads: {
    title: 'Tomato salads',
    image: '/img/salads.png',
    content: (
      <Box>
        This salads - our <StyledLink href='/'>2020 Recipe of the Year</StyledLink> - uses the natural ingredients, but is incredibly delicious.<br/>
        We often make the salads mayonnaise and fruits, and honestly? No one can tell, it's that good! And not only is this salads delicious - it's health foods.
      </Box>
    ),
    details: {
      prep: '20 mins',
      cook: '1 hr to 1 hr 15 mins',
      total: '1 hr 20 mins'
    },
    yield: '1 vessel, 3 generous servings'
  }
}

const RecipeCategory: NextPage<Props> = ({ error404 }) => {
  const router = useRouter();
  let path = router.asPath || '';
  if (path[0] === '/') {
    path = path.slice(1);
  }
  const category = path.split('/')[2];

  if (error404) {
    return <Error404Page />
  }

  return (
    <RecipeCard data={categories[category] as any} />
  )
};

export default RecipeCategory;
